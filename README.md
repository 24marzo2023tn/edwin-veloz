<p align="center">
 
</p>
<h1 align="center"> Sistema de cursos </h1> <br>

<p align="center">
  Plataforma para administrar y ver información de estudiantes y profesores
</p>

## Tabla de contenidos

- [Introducción](#introduction)
- [Funciones](#features)
- [Retroalimentación](#feedback)
- [Contribuidores](#contributors)
- [Proceso de construcción](#build-process)
- [Contacto](#acknowledgments)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Introducción

Este proyecto fue realizado como prueba tecnica para TELCONET, consta de una aplicación desplegada en Angular, que usa una base de datos MySQL y el framework para backend (SpringBoot).



## Funciones

Estas son algunas de las características del sistema de cursos:

* Registrar usuario como profesor
* Registrar usuario como estudiantes
* Ver información de profesor
* Ver información de estudiantes
* Editar información de profesor y estudiantes
* Filtrar para que la tabla muestre únicamente los profesores
* Eliminar estudiante o profesor
<h1></h1>


## Retroalimentación

Siéntete libre de comentarme tu experiencia utilizando este sistema, puedes escribir al siguiente correo: edwin06111998@gmail.com. Tus comentarios son importantes para seguir haciendo robusto este sistema.

## Contribuidores

Este proyecto ha sido desarrollado únicamente por mí (Edwin Veloz).

## Proceso de construcción

- Clona o descarga el repositorio
BACKEND
- Abrir la carpeta PRUEBA_PRACTICA/prueba_tecnica_backend/Spring-Backend-master/gestion-empleados-backend desde un IDE que soporte Springboot, en este caso, se usó Eclipse
- Si no tiene instalado SpringBoot en Eclipe, puede ejecutar el backend con los siguientes pasos:
- Clic derecho en la carpeta "gestion-empleados-backend", Run As, Maven Build
- Una vez dentro, setear el campo Goals con (spring-boot:run),y dar clic en finalizar, esto ejecutará el backend.
FRONTEND
- Tener instalado NodeJS en su última versión de preferencia
- Desde la terminal ubicar el directorio de la carpeta (PRUEBA_PRACTICA/prueba_tecnica_frontend)
- Ejecutar el comando (npm i) para instalar las dependecias
- Instalar angular mediante el comando npm install @angular/cli
- Ejecutar el frontend con el comando "ng serve -o"

Importante: el código se realizó en un ambiente Linux, por si existe alguna incompatibilidad entre sistemas operativos.

## Contacto

- LinkedIn: www.linkedin.com/in/edwin-veloz-2153a9137
- Correo: edwin06111998@gmail.com
