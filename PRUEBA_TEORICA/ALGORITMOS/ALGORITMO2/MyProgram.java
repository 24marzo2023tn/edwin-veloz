import java.util.Arrays;
class MyProgram {
        void CombSort(int numeros[]){
        int longitud_numeros = numeros.length;
        float shrinkFactor = 1.3f;
        boolean intercambiado = false;

        while (longitud_numeros > 1 || intercambiado) {
            if (longitud_numeros > 1) {
                longitud_numeros = (int)(longitud_numeros / shrinkFactor);
            }

            intercambiado = false;

            for (int i = 0; longitud_numeros + i < numeros.length; i++) {
                if (numeros[i] > numeros[i + longitud_numeros]) {
                    swap(numeros, i, i + longitud_numeros);
                    intercambiado = true;
                }
            }
        }
    }
     private static void swap(int numeros[], int x, int y) {
        Integer temp = numeros[x];
        numeros[x] = numeros[y];
        numeros[y] = temp;
    }
    public static void main(String args[])
    {
        MyProgram ob = new MyProgram();
        int numeros[] = {45, -20, 7, 2, 1, 0, 202};
        System.out.println("Original Array:");
        System.out.println(Arrays.toString(numeros));
        ob.CombSort(numeros);
        System.out.println("Sorted Array");
        System.out.println(Arrays.toString(numeros));
    }
}