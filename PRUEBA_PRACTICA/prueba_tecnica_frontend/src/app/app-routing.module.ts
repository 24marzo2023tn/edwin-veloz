import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActualizarEmpleadoComponent } from './actualizar-empleado/actualizar-empleado.component';
import { EmpleadoDetallesComponent } from './empleado-detalles/empleado-detalles.component';
import { ListaEmpleadosComponent } from './lista-empleados/lista-empleados.component';
import { ListaProfesoresComponent } from './lista-profesores/lista-profesores.component';
import { RegistrarEmpleadoComponent } from './registrar-empleado/registrar-empleado.component';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const routes: Routes = [
  {path: 'empleados', component:ListaEmpleadosComponent},
  {path: 'profesores', component:ListaProfesoresComponent},
  {path: '', redirectTo:'login',pathMatch:'full'},
  {path: 'login', component:LoginComponent},
  {path: 'registrar-empleado', component:RegistrarEmpleadoComponent},
  {path: 'actualizar-empleado/:id', component:ActualizarEmpleadoComponent},
  {path: 'empleado-detalles/:id', component: EmpleadoDetallesComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
