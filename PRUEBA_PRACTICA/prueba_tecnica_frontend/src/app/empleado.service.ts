import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, RepeatConfig } from 'rxjs';
import {Empleado} from './empleado'
import {LoginI} from './modelos/login.interface'
import {ResponseI} from './modelos/response.interface'

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  //Esta URL obtiene la lista de todos los empleados desde el backend
  private baseURL = "http://localhost:8080/api/v1/empleados"

  constructor(private httpClient : HttpClient) { }

  //Este metodo obtiene los empleados
  obtenerListaEmpleados() : Observable<Empleado[]>{
    return this.httpClient.get<Empleado[]>(`${this.baseURL}`);
  }

  //Este metodo registra un empleado
  registrarEmpleado(empleado:Empleado):Observable<Object>{
    return this.httpClient.post(`${this.baseURL}`,empleado)
  }

  //Este metodo actualiza un empleado
  actualizarEmpleado(id:Number, empleado:Empleado):Observable<Object>{
    return this.httpClient.put(`${this.baseURL}/${id}`,empleado);
  }

  //Este metodo obtiene un empleado
  obtenerEmpleado(id:Number):Observable<Empleado>{
    return this.httpClient.get<Empleado>(`${this.baseURL}/${id}`);
  }

  //Este metodo obtiene un empleado
  eliminarEmpleado(id:Number):Observable<Object>{
    return this.httpClient.delete(`${this.baseURL}/${id}`);
  }

  //Este metodo obtiene un empleado
  loginByEmail(form:LoginI):Observable<ResponseI>{
    return this.httpClient.post<ResponseI>(`${this.baseURL}/auth`,form)   
  }
}
