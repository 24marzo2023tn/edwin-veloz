export class Empleado {
    id:number;
    nombre:string;
    apellido:string
    email:string
    contrasena:string
    area:string
    especialidad:string
    tipo_usuario:string
    nivel:string
    motivo_registro:string
}
