import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {EmpleadoService} from '../empleado.service'
import {LoginI} from '../modelos/login.interface'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  loginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    contrasena: new FormControl('', Validators.required)
  })
  constructor(private api: EmpleadoService){

  }
  ngOnInit(): void {
      
  }

  onLogin(form ){
    this.api.loginByEmail(form).subscribe(data => {
      console.log(data)
    })
  }

}
