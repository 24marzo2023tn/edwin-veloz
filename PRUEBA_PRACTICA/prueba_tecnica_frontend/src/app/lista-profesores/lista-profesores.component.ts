import { Component, OnInit } from '@angular/core';
import { EmpleadoService } from '../empleado.service';
import {Empleado} from './../empleado'
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-profesores',
  templateUrl: './lista-profesores.component.html',
  styleUrls: ['./lista-profesores.component.css']
})
export class ListaProfesoresComponent implements OnInit{
  empleados: Empleado[];
  constructor(private empleadoServicio:EmpleadoService, private router:Router){}

  private obtenerEmpleados(){
    this.empleadoServicio.obtenerListaEmpleados().subscribe(dato => {
      this.empleados = dato;
    })
  }

  findProfesores(empleadoServicio:any[]): any[] {
    return empleadoServicio.filter(p => p.tipo_usuario == "Profesor");
  }

  actualizarEmpleado(id: Number){
    this.router.navigate(['actualizar-empleado',id]);
  }

  eliminarEmpleado(id: Number){
    this.empleadoServicio.eliminarEmpleado(id).subscribe(dato => {
      console.log(dato);
      this.obtenerEmpleados();
    })
  }

  verDetallesEmpleado(id: Number){
    this.router.navigate(['empleado-detalles',id]);
  }

  ngOnInit(): void {
    this.obtenerEmpleados();
  }
}
