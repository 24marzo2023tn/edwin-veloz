package com.gestion.empleados.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "empleados")
public class Empleado {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nombre", length = 60, nullable = false)
	private String nombre;

	@Column(name = "apellido", length = 60, nullable = false)
	private String apellido;

	@Column(name = "email", length = 60, nullable = false, unique = true)
	private String email;
	
	@Column(name = "contrasena", length = 60, nullable = false, unique = false)
	private String contrasena;
	
	@Column(name = "area", length = 60, nullable = false, unique = false)
	private String area;
	
	@Column(name = "especialidad", length = 100, nullable = false, unique = false)
	private String especialidad;
	
	@Column(name = "tipo_usuario", length = 60, nullable = false, unique = false)
	private String tipo_usuario;
	
	@Column(name = "nivel", length = 60, nullable = false, unique = false)
	private String nivel;
	
	@Column(name = "motivo_registro", length = 60, nullable = false, unique = false)
	private String motivo_registro;

	public Empleado() {

	}

	public Empleado(Long id, String nombre, String apellido, String email, String contrasena, String area,
			String especialidad, String tipo_usuario, String nivel, String motivo_registro) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.contrasena = contrasena;
		this.area = area;
		this.especialidad = especialidad;
		this.tipo_usuario = tipo_usuario;
		this.nivel = nivel;
		this.motivo_registro = motivo_registro;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public String getTipo_usuario() {
		return tipo_usuario;
	}

	public void setTipo_usuario(String tipo_usuario) {
		this.tipo_usuario = tipo_usuario;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getMotivo_registro() {
		return motivo_registro;
	}

	public void setMotivo_registro(String motivo_registro) {
		this.motivo_registro = motivo_registro;
	}

}
